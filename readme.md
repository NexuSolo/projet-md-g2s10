# Jeu De La Vie Unidimensionnel - G2S10

## Présentation rapide du jeu

Le jeu de la vie est un jeu qui représente des cellules dans une matrice, une cellule peut être soit vivante soit morte. Elles évoluent à chaque avancement dans le temps avec les règles suivantes:

* Une cellule vivante meurt si et seulement si au moins une de ses deux voisines est vivante.
* Une cellule morte naît si et seulement si exactement une de ses deux voisines est vivante.

## Prérequis

Le projet demande `Java 17.0.5.X` ou version plus récente, avec `JavaFX` de preference

Vous pouvez installer [Liberica JDK 17](https://bell-sw.com/pages/downloads/#/java-17-lts), Pour avoir JavaFx veuillez choisir le package `Full JRE` ou `Full JDK`

## Build le projet

1. Clonez le depot:`git clone https://gaufre.informatique.univ-paris-diderot.fr/theau/projet-md-g2s10`
2. Ce placer dans le dossier projet-md-g2s10: `cd projet-md-g2s10`
3. Build le projet `./gradlew build` le fichier compile se trouve dans le dossier `app/build/libs`

## Pour lancer le projet

1. Si vous avez `Java 17` avec `JavaFX` d'installer, vous pouvez double cliquer sur le fichier `JeuDeLaVie.jar`
2. Vous pouvez aussi executer la commande `./gradlew run` dans un terminal, depuis le dossier `projet-md-g2s10`

* Pour lancer la detection des configurations catastrophiques pour merci d'executer la commande `./gradlew -q nbrChao` dans un terminal, depuis le dossier `projet-md-g2s10`

**⚠️ Il est déconseillé de lancer le projet sans `JavaFX` ⚠️**

3. Si vous n'avez pas `JavaFX` d'installer, vous pouvez lancer le jeu avec :

   * Si vous êtes sur Windows, double cliquer sur le fichier `runWin.bat`
   * Si vous êtes sur Linux, double cliquer sur le fichier `runLinux.sh`
   * Si vous êtes sur Mac, double cliquer sur le fichier `runMacOs.sh`

*(si vous voulez lancer le projet que vous avez build [`./gradlew build`], copie le fichier `JeuDeLaVie.jar` _(qui se trouve dans le dossier `app/build/libs` [`cd app/build/libs`])_) dans le dossier `projet-md-g2s10`*

### Erreurs connues en moment de lancement

> Error: JavaFX runtime components are missing, and are required to run this application

Si vous avez cette erreur, vous n'avez pas `JavaFX` d'installer, veuillez suivre les instructions decrite dans:

* [Prérequis](#prérequis) pour l'installer, puis relancer le projet.
* [Pour lancer le projet](#pour-lancer-le-projet) avec les scripts fournis.

> Error: LinkageError occurred while loading main class projet.md.g2s10.App
> java.lang.UnsupportedClassVersionError: projet/md/g2s10/App has been compiled by a more recent version of the Java Runtime (class file version XX.XX), this version of the Java Runtime only recognizes class file versions up to XX.XX

Si vous avez cette erreur, vous avez une version de Java inferieur à `Java 17.0.5.X`, veuillez suivre les instructions decrite dans:

* [Prérequis](#prérequis) pour l'installer, puis relancer le projet.

## Explication de l'interface

### Les menus

#### Configurations

* On peut choisir la taille du tableau _(la mollete permet de choisir les nombres plus rapidement)_
* On peut modifier la couleur des cellules vivantes et mortes
* On peut lancer la simulation avec le bouton `Lancer le jeu`

#### Choix des cellules vivantes

* On peut choisir les cellules vivantes/mortes en cliquant sur les cases du cercle
* Pour choisir plus rapidement, on peut rester appuyé sur la souris et faire un glisser sur les cases du cercle

#### Pendant la simulation

* Les cases `Cycle`, `Stable` et `Catastrophique` permettent de savoir l'état final de la simulation
* Le button `Acceuil` permet de revenir au menu de [Configurations](#configurations)
* Le button `Suivat` permet de passer a la generation suivante
* Le button `Avance rapide` permet de lancer la simulation en continu
  * Le button `Stop` permet d'arrêter la simulation en continu
* On peut choisir la vitesse de la simulation en modifiant la valeur du slider `Vitesse par seconde` _(la mollete permet de choisir les nombres plus rapidement)_
* On a aussi la conaissance du nombre de generation actuel, le nombre de cellules vivantes et le nombre de cellules mortes

## Images

### Configuration

![Configuration.png](assets/Configuration.png)

### Choix des cellules

![ChoixCellule.png](assets/ChoixCellule.png)

### Simulation

![Simulation.png](assets/Simulation.png)

## Auteurs

* [Rene Tom 22007185](https://gaufre.informatique.univ-paris-diderot.fr/rene)
* [Gargaun Illia 22007415](https://gaufre.informatique.univ-paris-diderot.fr/gargaun)
* [Theau Nicolas 22011387](https://gaufre.informatique.univ-paris-diderot.fr/theau)
