package projet.md.g2s10;

import java.util.LinkedList;
import java.util.List;

public class ConfigurationChaotique {

    public static List<Boolean> nextEtape(List<Boolean> list) {
        List<Boolean> next = new LinkedList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean b = list.get(i);
            boolean b1 = list.get((i + 1) % list.size());
            boolean b2 = list.get((i - 1 + list.size()) % list.size());
            if(b) {
                if(b1 || b2) {
                    next.add(false);
                }
                else {
                    next.add(true);
                }
            }
            else {
                if(b1 ^ b2) {
                    next.add(true);
                }
                else {
                    next.add(false);
                }
            }
        }
        return next;
    }

    public static List<List<Boolean>> allConfig(int n) {
        List<List<Boolean>> list = new LinkedList<>();
        //renvoyer toutes les configartions possition de la liste de boolean
        for(int i = 0; i < Math.pow(2, n); i++) {
            List<Boolean> l = new LinkedList<>();
            for(int j = 0; j < n; j++) {
                l.add((i & (1 << j)) != 0);
            }
            list.add(l);
        }
        return list;
    }

    public static int nombreCata(List<List<Boolean>> list) {
        int nb = 0;
        for(List<Boolean> l : list) {
            List<Boolean> next = nextEtape(l);
            List<Boolean> faux = new LinkedList<>();
            for(int i = 0; i < l.size(); i++) {
                faux.add(false);
            }
            if(next.equals(faux)) {
                nb++;
            }
        }
        return nb;
    }
    
    public static void main(String[] args) {
        List<List<List<Boolean>>> list = new LinkedList<>();
        for(int i = 1; i <= 20; i++) {
            list.add(allConfig(i));
        }
        for(int i = 0; i < list.size(); i++) {
            System.out.println("Pour n = " + (i + 1) + " il y a " + nombreCata(list.get(i)) + " configurations catastrophiques");
        }
    }

}
