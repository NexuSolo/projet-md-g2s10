package projet.md.g2s10.Composant;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import projet.md.g2s10.App;
import projet.md.g2s10.Cellules;

public class ArcID extends Arc {
    private int id;

    public ArcID(double x, double y, double radius, double start, double end, int id, Cellules cellules) {
        super(x, y, radius, radius, start, end);
        this.id = id;

        this.setOnMousePressed(e->{
            App.editorDrag = true;
        });

        this.setOnDragDetected(e->{
             this.startFullDrag();
        });

        this.setOnMouseReleased(e->{
            App.editorDrag = false;
        });

        this.setOnMouseDragEntered(e->{
            if (App.editorDrag) {
                Color c = (getFill() == App.couleurVivant ? App.couleurMort : App.couleurVivant);
                this.setFill(c);
                cellules.setCellule(id, c == App.couleurVivant);
            }
        });

    }

    public int getIdCellule() {
        return id;
    }

}
