package projet.md.g2s10;

public class Status {
    private boolean catastrophic;
    private boolean cycle;
    private boolean stable;
    private boolean eden;

    public Status() {
        catastrophic = false;
        cycle = false;
        stable = false;
        eden = false;
    }

    public Status(Status status) {
        catastrophic = status.catastrophic;
        cycle = status.cycle;
        stable = status.stable;
        eden = status.eden;
    }

    public boolean isCatastrophic() {
        return catastrophic;
    }

    public void setCatastrophic(boolean catastrophic) {
        this.catastrophic = catastrophic;
    }

    public boolean isCycle() {
        return cycle;
    }

    public void setCycle(boolean cycle) {
        this.cycle = cycle;
    }

    public boolean isStable() {
        return stable;
    }

    public void setStable(boolean stable) {
        this.stable = stable;
    }

    public boolean isEden() {
        return eden;
    }

    public void setEden(boolean eden) {
        this.eden = eden;
    }


}
