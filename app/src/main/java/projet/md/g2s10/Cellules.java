package projet.md.g2s10;

import java.util.LinkedList;
import java.util.List;

public class Cellules {
    private final LinkedList<Boolean> list = new LinkedList<>();
    private final LinkedList<List<Boolean>> history = new LinkedList<>();
    private final int size;
    private boolean all_dead = false;
    private int in_life = 0;
    private int max_life;
    private final Status status = new Status();
    private final LinkedList<LinkedList<Boolean>> cycleList = new LinkedList<>();

    private int generation = 0;
    private boolean checkCycle = true;

    public void setCheckCycle(boolean checkCycle) {
        this.checkCycle = checkCycle;
    }

    public Cellules(int n) {
        this.size = n;
        for (int i = 0; i < n; i++) {
            list.add(false);
        }
        max_life = in_life;
        if (App.debug)
            System.out.println(this + " Generation: " + generation + " En vie: " + in_life + " Max en vie: " + max_life);
    }

    public void nextGen(LinkedList<Boolean> list, int nbGen) {
        boolean tmp = true;
        if (!all_dead) {
            int tmp_max = 0;
            LinkedList<Boolean> old = new LinkedList<>(list);
            if (list != this.list) {
                list = new LinkedList<>(list);
            } else {
                history.add(new LinkedList<>(list));
            }
            for (int i = 0; i < list.size(); i++) {
                int voisins = 0;
                int x = i;
                if (x >= size - 1) {
                    x = -1;
                }
                if (old.get(x + 1)) {
                    voisins++;
                }
                x = i;
                if (x == 0) {
                    x = size;
                }
                if (old.get(x - 1)) {
                    voisins++;
                }

                if (old.get(i) && voisins >= App.deathIf) {
                    list.set(i, false);
                } else if (voisins == App.birthIf) {
                    list.set(i, true);
                }

                if (list.get(i)) {
                    tmp = false;
                    tmp_max++;
                }
            }
            if (list == this.list) {
                generation++;
                max_life = Integer.max(max_life, tmp_max);
                in_life = tmp_max;
                if (App.debug)
                    System.out.println(this + " Generation: " + generation + " En vie: " + in_life + " Max en vie: " + max_life);
                if (tmp) {
                    all_dead = true;
                    if (App.debug) {
                        System.out.println("Tout le monde est mort a la generation: " + generation);
                        System.out.println("Max en vie: " + max_life);
                    }
                }
            } else {
                if (nbGen == 0) {
                    status.setStable(compare2List(list, old));
                    status.setCatastrophic(tmp);
                } else {
                    LinkedList<Boolean> list2 = new LinkedList<>(list);
                    if (cycleList.stream().anyMatch(l2 -> compare2List(l2, list2))) {
                        System.out.println("C'est un cycle " + nbGen);
                        System.out.println(" cycle :" + list.stream().map(b -> b ? "#" : "-").reduce("", (s, s2) -> s + s2));
                        status.setCycle(true);
                    } else {
                        if (tmp) {
                            status.setCycle(false);
                            return;
                        }
                        cycleList.add(list2);
                        nextGen(list2, nbGen - 1);
                    }
                }
            }
        } else {
            status.setCatastrophic(true);
            status.setStable(true);
        }
    }

    public synchronized void update() {
        nextGen(list, 0);
        testNextGen();
    }

    public void testNextGen() {
        nextGen(new LinkedList<>(list), 0);
    }

    public void isCycle() {
        if (!status.isCycle() && checkCycle) {
            LinkedList<Boolean> list = new LinkedList<>(this.list);
            System.out.println("is cycle : " + (size * size));
            nextGen(list, size * size);
            cycleList.removeAll(cycleList);
        }
    }

    public boolean isAll_dead() {
        return all_dead;
    }

    public LinkedList<List<Boolean>> getHistory() {
        return new LinkedList<>(history);
    }

    public int getIn_life() {
        return in_life;
    }

    public int getDead() {
        return size - in_life;
    }

    public int getMax_life() {
        return max_life;
    }

    public int getGeneration() {
        return generation;
    }

    public LinkedList<Boolean> getOldList() {
        return new LinkedList<>(history.getLast());
    }

    public int getSize() {
        return size;
    }

    public LinkedList<Boolean> getList() {
        return new LinkedList<>(list);
    }

    @Override
    public String toString() {
        return list.stream().map(b -> b ? "#" : "-").reduce("", (s, s2) -> s + s2);
    }

    public static boolean compare2List(LinkedList<Boolean> l1, LinkedList<Boolean> l2) {
        if (l1.size() != l2.size()) {
            return false;
        }
        for (int i = 0; i < l1.size(); i++) {
            if (l1.get(i) != l2.get(i)) {
                return false;
            }
        }
        return true;
    }

    public void setCellule(int i, boolean b) {
        if (b && !list.get(i)) {
            in_life++;
        } else if (!b && list.get(i)) {
            in_life--;
        }
        list.set(i, b);
        max_life = Integer.max(max_life, in_life);
    }

    public Status getStatus() {
        return status;
    }
}
