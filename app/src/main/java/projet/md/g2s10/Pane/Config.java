package projet.md.g2s10.Pane;

import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import projet.md.g2s10.App;
import projet.md.g2s10.Cellules;
import projet.md.g2s10.Composant.ArcID;
import projet.md.g2s10.Controller.JeuController;

import java.util.LinkedList;

public class Config extends AnchorPane {
    private Cellules cellules;
    private LinkedList<ArcID> arcs = new LinkedList<>();

    private boolean editable = true;

    private Text text = new Text();
    public Config(Cellules cellules, boolean editable) {
        super();
        this.cellules = cellules;
        Circle vide = new Circle(310, 225, 190);
        vide.setFill(javafx.scene.paint.Color.GRAY);
        int i = 0;
        double pas = (double) 360 / cellules.getList().size();
        for (boolean b : cellules.getList()) {
            ArcID arc = new ArcID(310, 225, 220, i * pas, pas, i++, cellules);
            arcs.add(arc);
            arc.setFill(b ? App.couleurVivant : App.couleurMort);
            arc.setStroke(javafx.scene.paint.Color.BLACK);
            arc.setStrokeType(StrokeType.CENTERED);
            arc.setType(ArcType.ROUND);
            arc.setOnMouseClicked(e->{
                if(editable) {
                    ArcID arcID = (ArcID) e.getTarget();
                    Color c = (arcID.getFill() == App.couleurVivant ? App.couleurMort : App.couleurVivant);
                    arcID.setFill(c);
                    cellules.setCellule(arcID.getIdCellule(), c == App.couleurVivant);
                    cellules.testNextGen();
                }
            });

            arc.setOnMouseDragEntered(e->{
                if(editable) {
                    ArcID arcID = (ArcID) e.getTarget();
                    if (App.editorDrag) {
                        Color c = (arcID.getFill() == App.couleurVivant ? App.couleurMort : App.couleurVivant);
                        arcID.setFill(c);
                        cellules.setCellule(arcID.getIdCellule(), c == App.couleurVivant);
                    }
                }
            });
            this.getChildren().add(arc);
        }
        addEvent(vide);
        addEvent(this);
        this.getChildren().add(vide);
        text.setFont(new Font(15));
        text.setFill(Color.WHITE);
        this.getChildren().add(text);
    }

    private void addEvent(Node node){
        node.setOnMousePressed(e->{
            App.editorDrag = true;
        });

        node.setOnDragDetected(e->{
            node.startFullDrag();
        });

        node.setOnMouseReleased(e->{
            App.editorDrag = false;
        });
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public Text getText() {
        return text;
    }

    public void updateText(String s) {
        text.setText(s);

        text.setX(310 - text.getLayoutBounds().getWidth() / 2);
        text.setY(225 - text.getLayoutBounds().getHeight()/4);
    }

    public void update() {
        int i = 0;
        for (boolean b : cellules.getList()) {
            arcs.get(i++).setFill(b ? App.couleurVivant : App.couleurMort);
        }
    }

}
