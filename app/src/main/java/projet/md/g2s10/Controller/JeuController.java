package projet.md.g2s10.Controller;

import java.io.IOException;
import java.util.LinkedList;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import projet.md.g2s10.App;
import projet.md.g2s10.Status;
import projet.md.g2s10.Pane.Config;

public class JeuController {

    private Config config;

    int min = 1;
    int max = 15;
    int start = 2;

    @FXML
    private AnchorPane panelCellules;

    @FXML
    private Text textEtape;

    @FXML
    private CheckBox boxCycle;

    @FXML
    private Spinner<Integer> speedField;

    @FXML
    private CheckBox boxStable;

    @FXML
    private CheckBox boxCatastrophique;

    @FXML
    private Button buttonSuivant;

    @FXML
    private Button buttonAvanceRapide;

    @FXML
    private Button buttonAcceuil;

    @FXML
    private Text textVivante;

    @FXML
    private Text textMorte;

    @FXML
    private Text textNombreVivant;

    @FXML
    private Text textNombreMorte;

    @FXML
    private void initialize() {
        config = new Config(App.cellules,false);
        App.cellules.nextGen(new LinkedList<>(App.cellules.getList()),0);
        App.cellules.isCycle();
        Status status = App.cellules.getStatus();
        boxCatastrophique.setSelected(status.isCatastrophic());
        boxCycle.setSelected(status.isCycle());
        boxStable.setSelected(status.isStable());
        
        panelCellules.getChildren().add(config);
        SpinnerValueFactory<Integer> factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(min, max, start);
        factory.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue < min) {
                factory.valueProperty().setValue(min);
            } else if (newValue > max) {
                factory.valueProperty().setValue(max);
            }
        });
        factory.setWrapAround(true);
        speedField.setValueFactory(factory);
        textVivante.setStyle("-fx-fill: " + App.couleurVivant.toString().replace("0x", "#"));
        textMorte.setStyle("-fx-fill: " + App.couleurMort.toString().replace("0x", "#"));
        textNombreVivant.setText(App.cellules.getIn_life() + "");
        textNombreMorte.setText(App.cellules.getDead() + "");
    }

    @FXML
    private void acceuil() throws IOException {
        App.setRoot("Config");
    }

    @FXML
    private void suivant() {
        updateGen();
    }

    @FXML
    private void avanceRapide() {
        App.avanceRapide = !App.avanceRapide;
        buttonSuivant.setDisable(App.avanceRapide);
        buttonAcceuil.setDisable(App.avanceRapide);
        buttonAvanceRapide.setText(App.avanceRapide ? "Stop" : "Avance rapide");
        Worker worker = new Worker();
        App.executorService.execute(worker);

    }

    private void updatePanel(){
        config.update();
        updateModifiable();
    }

    public void updateModifiable(){
        textEtape.setText(App.cellules.getGeneration() + "");
        textNombreVivant.setText(App.cellules.getIn_life() + "");
        textNombreMorte.setText(App.cellules.getDead() + "");
        Status status = App.cellules.getStatus();
        boxCatastrophique.setSelected(status.isCatastrophic());
        boxCycle.setSelected(status.isCycle());
        boxStable.setSelected(status.isStable());
    }

    private void updateGen(){
        App.cellules.update();
        updatePanel();
        if(App.cellules.isAll_dead()){
            config.updateText("Tout le monde est mort a la generation: " + App.cellules.getGeneration() +"\nMaximum de cellule en vie: " + App.cellules.getMax_life());
            buttonSuivant.setDisable(true);
            buttonAvanceRapide.setDisable(true);
            boxCatastrophique.setSelected(true);
            speedField.setDisable(true);
            buttonAcceuil.setDisable(false);
        }
    }

    private class Worker extends Task<Void> {
        @Override
        protected Void call(){
            while(App.avanceRapide && !App.cellules.isAll_dead()){
                updateGen();
                try {
                    Thread.sleep((1000 / speedField.getValue()));
                } catch (InterruptedException ignored) {
                    App.avanceRapide = false;
                }
            }
            return null;
        }
    }

    @FXML
    private void scrollMouse(ScrollEvent event) {
        if(event.getDeltaY() > 0){
            speedField.increment();
        }else{
            speedField.decrement();
        }
    }

}
