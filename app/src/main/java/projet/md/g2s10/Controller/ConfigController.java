package projet.md.g2s10.Controller;

import java.io.IOException;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import projet.md.g2s10.App;
import projet.md.g2s10.Cellules;
import projet.md.g2s10.Composant.ArcID;
import projet.md.g2s10.Pane.Config;

public class ConfigController {
    int oldTaille = 10;
    int taille = 10;
    Cellules cellules = new Cellules(taille);
    int min = 2;
    int max = 200;
    int start = 10;

    @FXML
    private TabPane tabPane;

    @FXML
    private Spinner<Integer> TailleSpinner;

    @FXML
    private Tab choixPane;

    @FXML
    private ColorPicker couleurVivante;

    @FXML
    private ColorPicker couleurMorte;

    @FXML
    private void initialize() {
        SpinnerValueFactory<Integer> factory = new SpinnerValueFactory.IntegerSpinnerValueFactory(min, max, start);
        factory.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue < min) {
                factory.valueProperty().setValue(min);
            } else if (newValue > max) {
                factory.valueProperty().setValue(max);
            }
        });
        factory.setWrapAround(true);
        TailleSpinner.setValueFactory(factory);
        taille = TailleSpinner.getValue();
        Tab t = tabPane.getTabs().get(1);
        t.setContent(new Config(cellules,true));
        t.getContent().setStyle("-fx-background-color: #54585d");
    }

    @FXML
    private void updateCellules() {
        TailleSpinner.commitValue();
        taille = TailleSpinner.getValue();
        if (taille != oldTaille) {
            cellules = new Cellules(taille);
            Tab t = tabPane.getTabs().get(1);
            t.setContent(new Config(cellules,true));
            t.getContent().setStyle("-fx-background-color: #54585d");
            oldTaille = taille;
        }
    }

    @FXML
    private void valider() throws IOException {
        App.cellules = cellules;
        taille = TailleSpinner.getValue();
        if(taille > 50){
            App.cellules.setCheckCycle(false);
        }
        if(taille != oldTaille) {
            App.cellules = new Cellules(taille);
            oldTaille = taille;
        }
        App.setRoot("Jeu");
    }

    @FXML
    private void couleurVivantUpdate(Event e) {
        App.couleurVivant = couleurVivante.getValue();
        updateColor();
    }
    
    @FXML
    private void couleurMorteUpdate(Event e) {
        App.couleurMort = couleurMorte.getValue();
        updateColor();
    }

    private void updateColor() {
        AnchorPane p = (AnchorPane) tabPane.getTabs().get(1).getContent();
        for (int i = 0; i < p.getChildren().size(); i++) {
            if (p.getChildren().get(i) instanceof ArcID) {
                ArcID a = (ArcID) p.getChildren().get(i);
                a.setFill(cellules.getList().get(a.getIdCellule()) ? App.couleurVivant : App.couleurMort);
            }
        }
    }

    @FXML
    private void scrollMouse(ScrollEvent event) {
        if(event.getDeltaY() > 0){
            if(TailleSpinner.getValue() >= max){
                TailleSpinner.getValueFactory().setValue(min);
            }else {
                TailleSpinner.increment();
            }
        }else{
            TailleSpinner.decrement();
        }
    }

}
